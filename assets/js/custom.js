jQuery(document).ready(function ($) {
    function custommatchHeight() {
        var options = ({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
        $('.why-choose-us__block .title h4').matchHeight(options);
        $('.amenities__block').matchHeight(options);
        $('.contact-address .card').matchHeight(options);
        $('.activities__content').matchHeight(options);

    }

    custommatchHeight()

    
    // svg image to code 
    jQuery('img.svg-icon').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

    $('.roomSlider').slick({
        dots: false,
    infinite: true,
    // speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplaySpeed: 3000,
    arrows: true,
    prevArrow: '<button class="slick-prev slick-arrow prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>',
    nextArrow: '<button class="slick-next slick-arrow next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>',
    });

    
    $('.banner__slider').slick({
        autoplay: false,
        speed: 800,
        lazyLoad: 'progressive',
        arrows: false,
        dots: false,
        prevArrow: '<button class="slick-prev slick-arrow prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slick-next slick-arrow next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>',
    });

    
    //   nav show and hide
    $('body').on('click', '.navbar-toggler', function () {
        $(this).toggleClass('on');
        $('.navbar-collapse').toggleClass('slide');
    });


    function fixHeaderSpacing() {
        var headerHeight = $('.navbar').outerHeight();
        $('body').css({
            'padding-top': headerHeight,
        });
    }
    fixHeaderSpacing();
    
    // dynamic year

    const paragraph = `
      
    &copy; ${new Date().getFullYear()}  PATLEKHET ECO FARMHOUSE 

`;

 document.getElementById('copyright').innerHTML = paragraph;

});

jQuery(window).on("load", function () {
    var $ = jQuery
  
    $('body').addClass('loaded');
    $('.loader').fadeOut('2000');
  });